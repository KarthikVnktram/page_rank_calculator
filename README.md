For this project, we used AWS Elastic MapReduce to compute PageRank for a reasonably large Web graph (685230 nodes, 7600595 edges, the “web-BerkStan” Web graph from the Stanford Large Network Dataset Collection). We computed PageRank on this graph using several different algorithms and compared their performance. 

The algorithms included a simple node-by-node approach [ Regular Page Rank Calculator] , a Blocked approached (which helped convergence to be reached faster) and more improved methods

Please refer to the README.pdf present in the source folder. This contains the explanation of logic of the code, how to run, how to build the jar, results of experimentation and other details.