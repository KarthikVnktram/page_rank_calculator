package GaussSeidelBlockMR;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class GaussSeidelBlockPRReduce extends Reducer<LongWritable, Text, Text, Text> {

	public static final double CONVERGENCE_SCALING_FACTOR = 100000.0;
	public static final double DAMPING_FACTOR = 0.85;
	
	//might need temporary pagerank value array
	public static enum Counter {
		ERROR_MARGIN,
		REDUCER_ITERATIONS
	}
	
	public void reduce(LongWritable key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
		Map<Long,Double> currentPageRankMap= new HashMap<Long,Double>();
		Map<Long, ArrayList<Long>> BE = new HashMap<Long, ArrayList<Long>>();
		Map<Long, Double > BC = new HashMap<Long, Double>();
		Map<Long, GaussSeidelBlockNode> currentNodesInBlockMap= new HashMap<Long, GaussSeidelBlockNode>(); 
		
		for(Text textValue : values){
			String textValueString = textValue.toString();
			String[] splitValues = textValueString.split(GaussSeidelBlockPageRankUtils.INPUT_TEXT_DELIMITER);
			String classifierString = splitValues[0];
			int indexOfFirstSpace = textValueString.indexOf(GaussSeidelBlockPageRankUtils.INPUT_TEXT_DELIMITER);
			
			if(classifierString.equals(GaussSeidelBlockPageRankUtils.CURRENT_PAGE_RANK)){
				GaussSeidelBlockNode node = new GaussSeidelBlockNode();
				node = node.getNodeFromText(textValueString.substring(indexOfFirstSpace+1, textValueString.length()), node);
				currentPageRankMap.put(node.getNodeId(), node.getPageRank());
				currentNodesInBlockMap.put(node.getNodeId(), node);
			
			} else if(classifierString.equals(GaussSeidelBlockPageRankUtils.CURRENT_BLOCK_EDGES)){
				long fromNode = Long.valueOf(splitValues[1]);
				long toNode = Long.valueOf(splitValues[2]);
				
				if(BE.containsKey(toNode) == false){
					BE.put(toNode, new ArrayList<Long>());
				}
				ArrayList<Long> nodesinSameBlock = BE.get(toNode);
				nodesinSameBlock.add(fromNode);
				BE.put(toNode,nodesinSameBlock);
			
			} else if(classifierString.equals(GaussSeidelBlockPageRankUtils.BOUNDARY_CONDITION)){
				long fromNode = Long.valueOf(splitValues[1]);
				//fromNode data is not needed ! Just keeping it constant with Doc, where they include 
				//fromNode which is from another block in the tuple
				long toNode = Long.valueOf(splitValues[2]);
				double pageRankReceived = Double.valueOf(splitValues[3]);
				
				if(BC.containsKey(toNode) == false){
					BC.put(toNode, 0.0);
				}
				double currentPR = BC.get(toNode);
				currentPR = currentPR+ pageRankReceived;
				BC.put(toNode,currentPR);
			
			} else {
				System.out.println("something wrong !! PR BE BC key not found ! Mapper Bug may be");
			}
		}
		
		
		//Lets run for a given number of iterations
		int iterationsToRun = 50;
		int iterationCounter = 1;
		double residualErrorAccepted = 0.001;
		double currentResidualError = Double.MAX_VALUE;
		while(currentResidualError > residualErrorAccepted){
			if(iterationCounter == iterationsToRun){
				break;
			}
			currentResidualError = iterateBlockOnce(currentNodesInBlockMap,currentPageRankMap,BE,BC);
			iterationCounter ++;
		}
		System.out.println("Iteration Counter for block "+key+" is "+ iterationCounter);
		
		/*You need to compute and output the residual for the entire Block Reducer computation, 
		which may consist of many calls to IterateBlockOnce(). 
		This is the computation involving PRstart(v) and PRend(v) discussed in Section 5 above. 
		You will need this to use as a global stopping condition for the MapReduce passes.
		*/
		// old Page Rank is stored in the currentNode in Block Map
		// current page rank is present in the currentPageRankMap
		// calculate residual using this ! and output data such that mapper can read it and continue !
		double residualToPutinCounter = 0.0;
		
		for(long nodeId : currentNodesInBlockMap.keySet()){
			GaussSeidelBlockNode node = currentNodesInBlockMap.get(nodeId);
			double pageRankStart = node.getPageRank();
			double pageRankEnd = currentPageRankMap.get(nodeId);
			double perNodeResidualError = Math.abs(pageRankStart-pageRankEnd)/pageRankEnd;
			residualToPutinCounter = residualToPutinCounter + perNodeResidualError;

		}
		double normalizedResidualToPutInCounter = residualToPutinCounter/currentNodesInBlockMap.size();
		//System.out.println(" Residual added from block "+key+"  is  "+normalizedResidualToPutInCounter);
		long scaledResidual = (long)(normalizedResidualToPutInCounter * CONVERGENCE_SCALING_FACTOR);
		
		context.getCounter(Counter.ERROR_MARGIN).increment(scaledResidual);
		
		context.getCounter(Counter.REDUCER_ITERATIONS).increment(iterationCounter);
		
		
		long minimumNodeId = Long.MAX_VALUE;
		long secondMinimumNodeId =Long.MAX_VALUE;
		for(long nodeId : currentNodesInBlockMap.keySet()){
			//emit the new data with saved page rank
			
			if(nodeId < minimumNodeId ){
				secondMinimumNodeId = minimumNodeId;
				minimumNodeId = nodeId;
			} //If nodeID is in between first and second
              //then update second 
			else if (nodeId < secondMinimumNodeId && nodeId != minimumNodeId) {
				secondMinimumNodeId = nodeId;
			}
		    
			GaussSeidelBlockNode node = currentNodesInBlockMap.get(nodeId);
			node.setPageRank(currentPageRankMap.get(nodeId));
			context.write(new Text(node.serializeNode()),new Text()) ;
		}
		System.out.println("Block :"+key + " Minimum Node ID "+minimumNodeId +" Page Rank :"+currentNodesInBlockMap.get(minimumNodeId).getPageRank());
		System.out.println("Block :"+key + " Second Minimum Node ID "+secondMinimumNodeId +" Page Rank :"+currentNodesInBlockMap.get(secondMinimumNodeId).getPageRank());
		System.out.print("\n");
	}

	private double iterateBlockOnce(Map<Long, GaussSeidelBlockNode> currentNodesInBlockMap, Map<Long, Double> currentPageRankMap, Map<Long, ArrayList<Long>> BE, Map<Long, Double> BC) {
		// TODO Auto-generated method stub
		
		double residualError = 0.0;
		for(long nodeId : currentNodesInBlockMap.keySet() ){
			double previousPageRankOfNode = currentPageRankMap.get(nodeId);
			double newPageRank = 0.0;
			// implementing below logic
			//for( u where <u, v> ∈ BE ) {
	        //    NPR[v] += PR[u] / deg(u);
	        //}
			if(BE.containsKey(nodeId)){
				for(long neighbourContributingNodeId: BE.get(nodeId)){
					GaussSeidelBlockNode neighboringNode = currentNodesInBlockMap.get(neighbourContributingNodeId);
					newPageRank = newPageRank + (currentPageRankMap.get(neighbourContributingNodeId) / neighboringNode.getNeighbourNodeIds().size());
				}
			}
			
			//Implementing Below Logic
			//for( u, R where <u,v,R> ∈ BC ) {
	        //    NPR[v] += R;
	        //}
			if(BC.containsKey(nodeId)){
				newPageRank = newPageRank + BC.get(nodeId);
			}
			
			 //NPR[v] = d*NPR[v] + (1-d)/N;
			newPageRank = (DAMPING_FACTOR * newPageRank) + ((1-DAMPING_FACTOR) / GaussSeidelBlockPageRankUtils.TOTAL_NODES_IN_GRAPH);
			currentPageRankMap.put(nodeId, newPageRank);
			
			residualError = residualError + Math.abs(previousPageRankOfNode-newPageRank)/newPageRank;
		}
		
		//implementing below code
		//for( v ∈ B ) { PR[v] = NPR[v]; }
		double normalizedResidualError = residualError/currentNodesInBlockMap.size();
		return normalizedResidualError;
	}
	
}

