package RandomPartitioning;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class RandomJacobiBlockPRMap extends Mapper<Text, Text, LongWritable, Text>{
	
	@Override
	protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
		
		RandomJacobiBlockNode node = new RandomJacobiBlockNode();
		node = node.getNodeFromText(key.toString(),node);
		//value is PR followed by Node text serialized
		String mapperValue = RandomJacobiBlockPageRankUtils.CURRENT_PAGE_RANK+RandomJacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+node.serializeNode();
		context.write(new LongWritable(node.getBlockId()),new Text(mapperValue));	
		if(node.getNeighbourNodeIds() != null && node.getNeighbourNodeIds().size() > 0 ) {
			String neigbourNodesMapperValue = null; 
					
			for(long neighbourNodeId: node.getNeighbourNodeIds()){
				if(neighbourNodeId == Long.MIN_VALUE){ // this indicates a sink node. node without any outgoing edges. ignore it
		    		break;	
		    	}
				//put the logic to handle the Neighboring nodes within same block and neighboring nodes outside same block here
				long blockId = getBlockIdForNodeId(neighbourNodeId);
				
				if(blockId == node.getBlockId()){ //neighbor is in same bock BE case
					//emit BE fromNodeID toNodeID 
					//This would be caught in reducer and a map like  <to nodeid -> all the fromNodeIds> is formed
					neigbourNodesMapperValue = RandomJacobiBlockPageRankUtils.CURRENT_BLOCK_EDGES+RandomJacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+
												node.getNodeId()+RandomJacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+
												neighbourNodeId;
				} else {
					//neighbor is in different block ! BC case
					//emit fromNodeID toNodeID PageRankValue
					//This would be caught in reducer and a map like  <to NodeId -> Sum Of PageRank From All Nodes Outside block> is formed
					double pageRankGoingOutOfBlock = node.getPageRank()/node.getNeighbourNodeIds().size();
					neigbourNodesMapperValue = RandomJacobiBlockPageRankUtils.BOUNDARY_CONDITION+RandomJacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+
												node.getNodeId()+RandomJacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+
												neighbourNodeId+RandomJacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+pageRankGoingOutOfBlock;
				}
				context.write(new LongWritable(blockId),new Text(neigbourNodesMapperValue));
			}
			
			
		}
	}
	
	//try to get a faster way to find blockIDs
	public long getBlockIdForNodeId(long nodeId){

		Random generator = new Random(nodeId);
		
		return ((nodeId * 71 + Math.abs(generator.nextInt()) ) % 68);

	}
	
	

}
