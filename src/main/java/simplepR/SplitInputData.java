package simplepR;
import java.io.*;
import java.util.*;

import JacobiBlockMR.JacobiBlockPageRankUtils;
import iterativeMR.PageRankUtils;
public class SplitInputData {
	private static void chunk_split(String inputFilePath, String outputFilePath) throws IOException{
	/*	FileInputStream blockFileInputStream=new FileInputStream("edgesAndBlocksFolder/blocks.txt");
		byte[] blockBuffer = new byte[7];
		String blocks = "";
		List<Long> blockList = new ArrayList<Long>();
		long previousBlockRange = 0l;
		while ((blockFileInputStream.read(blockBuffer)) > 0) {
			for (byte b : blockBuffer) {
				blocks += (char) b;
			}
			long currentBlockValue = Long.parseLong(blocks.trim());
			blockList.add(currentBlockValue+previousBlockRange);
			previousBlockRange = currentBlockValue;
			Arrays.fill(blockBuffer, (byte) 0);
			blocks ="";
		}*/
		



		FileInputStream fr=new FileInputStream(inputFilePath);
		byte[] buffer = new byte[26];
		String result = "";

		double fromNetID = 0.332; // kv233
		double rejectMin = 0.9 * fromNetID;
		double rejectLimit = rejectMin + 0.01;
		
		System.out.println("rejectMin"+ rejectMin);
		System.out.println("rejectLimit"+ rejectLimit);

		File targetFile = new File(outputFilePath);
		OutputStream outStream = new FileOutputStream(targetFile);

		Map<Long,List<Long>> adjList=new LinkedHashMap<Long,List<Long>>();

		while ((fr.read(buffer)) > 0) {
			for (byte b : buffer) {
				result += (char) b;
			}
			Arrays.fill(buffer, (byte) 0);
			//System.out.println(result.substring(0,6).trim() +"--" + result.substring(8,14).trim() +"--"+ result.substring(15,26).trim());
			double val=Double.parseDouble(result.substring(15,26).trim());
			boolean saveData =( ((val >= rejectMin) && (val < rejectLimit)) ? false : true );
			long src =Long.parseLong(result.substring(0,6).trim());
			long dst=Long.parseLong(result.substring(7,13).trim());
			if(saveData){


				if(adjList.containsKey(src) == false){
					adjList.put(src, new ArrayList<Long>());
				}
				List<Long> destinationNodes = adjList.get(src);
				destinationNodes.add(dst);
				adjList.put(src,destinationNodes);
				//				
			}


			result="";
		}
		String REGULAR_DELIMITER= " ";
		String DESTINATION_NODE_DELIMITER =",";
		double pageRank = 1.0/PageRankUtils.TOTAL_NODES_IN_GRAPH;
		
		for(int i=0; i<PageRankUtils.TOTAL_NODES_IN_GRAPH ; i++){
			long nodeId=i;
			long blockId = getBlockIdForNodeId(nodeId,JacobiBlockPageRankUtils.blockIndices);
			List<Long> destinationNodes = null;
			if(adjList.containsKey(nodeId) == true){

				destinationNodes = adjList.get(nodeId);
				
			} else{

				destinationNodes = new ArrayList<Long>();
				destinationNodes.add(Long.MIN_VALUE);
			}
			
			
			StringBuilder stringBuilder = new StringBuilder();
			
			for(int j =0 ; j< destinationNodes.size(); j++){
				long destNode = destinationNodes.get(j);
				stringBuilder.append(destNode);
				if(j != destinationNodes.size()-1){
					stringBuilder.append(DESTINATION_NODE_DELIMITER);
				} else {
					// do not add a ,
				}
				
			}
			
			String destNodeString = stringBuilder.toString();
			
			String modifiedData = nodeId+REGULAR_DELIMITER+blockId+REGULAR_DELIMITER+destNodeString+REGULAR_DELIMITER+pageRank+"\n";
			outStream.write(modifiedData.getBytes());

		}


		System.out.println("Input Generated");
		outStream.close();
		fr.close();
		//blockFileInputStream.close();
	}

	public static long getBlockIdForNodeId(long nodeId, long[] blockList){

		long low = 0l;
		long high = blockList.length-1;
		while(low != high){

			long mid = low+(high-low)/2;
			if(blockList[(int) mid] <= nodeId){
				low = mid+1;
			}else {
				high = mid ;
			} 
		}
		return low;

	}

	public static void main(String[] args) throws IOException{

		try{
		    String inputFilePath = args[0];
		    String outputFilePath = args[1];
		    // org.apache.log4j.BasicConfigurator.configure();
			chunk_split(inputFilePath,outputFilePath);
		} catch (Exception exception){
			System.out.println("Invalid input arguments ! : Format is inputfilePath outputFilePath");
			
		}
		
	}


}

