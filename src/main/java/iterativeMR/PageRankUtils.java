package iterativeMR;

import java.util.ArrayList;
import java.util.List;

public class PageRankUtils {
	public static String INPUT_TEXT_DELIMITER= " ";
	public static String ADJACENCY_LIST_DELIMITER =",";
	public static int TOTAL_NODES_IN_GRAPH = 685230;
	
	
	public static String convertLongListToString(List<Long> neighbourNodeIds){
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for(Long neighbourNode : neighbourNodeIds){
				stringBuilder.append(neighbourNode);
				if(neighbourNode == neighbourNodeIds.get(neighbourNodeIds.size()-1) == false){
					stringBuilder.append(PageRankUtils.ADJACENCY_LIST_DELIMITER);
				}
		}
		return stringBuilder.toString();
		
	}
	
	//nodeID blockID list,of,adj,nodeids pageRank
	public static Node getNodeFromText(String inputText){
		
		String[] splitText = inputText.split(PageRankUtils.INPUT_TEXT_DELIMITER);
		long nodeId = Long.parseLong(splitText[0]);
		long blockId = Long.parseLong(splitText[1]);
		
		String[] adjList = splitText[2].split(PageRankUtils.ADJACENCY_LIST_DELIMITER);
		List<Long> adjacencyList = new ArrayList<Long>();
		//try to use stream API of java 8
		for(String destNode:adjList){
			adjacencyList.add(Long.parseLong(destNode));
		}
		
		double pageRank = Double.parseDouble(splitText[3]);
		
		Node node = new Node(nodeId, blockId, adjacencyList, pageRank);
		
		return node;
		
	}
}
