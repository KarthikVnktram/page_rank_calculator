package iterativeMR;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class PRMap extends Mapper<Text, Text, LongWritable, Text>{
	
	@Override
	protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
		
		Node node = PageRankUtils.getNodeFromText(key.toString());
		context.write(new LongWritable(node.getNodeId()),new Text(node.serializeNode()));
		
		double distributedPageRank = node.pageRank /node.neighbourNodeIds.size()* 1.0;
		
		if(node.getNeighbourNodeIds() != null && node.getNeighbourNodeIds().size() > 0 ) {
	
		    for (long neighbour: node.getNeighbourNodeIds()) {
		    	
		    	if(neighbour == Long.MIN_VALUE){
		    		break;
		    		
		    	}
		    	
		    	context.write(new LongWritable(neighbour), new Text(String.valueOf(distributedPageRank)));
			  }
		}
	}
	
	

}
