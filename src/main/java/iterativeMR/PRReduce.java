package iterativeMR;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class PRReduce extends Reducer<LongWritable, Text, Text, Text> {

	public static final double CONVERGENCE_SCALING_FACTOR = 100000.0;
	public static final double DAMPING_FACTOR = 0.85;

	
	public static enum Counter {
		ERROR_MARGIN;
	}


	
	public void reduce(LongWritable key, Iterable<Text> values,Context context) throws IOException, InterruptedException {
	
		
		double aggregatePageRank = 0;

		Node node = null;
		
		for (Text textValue : values) {
			if(isNode(textValue)){ //this indicates a node
				node = PageRankUtils.getNodeFromText(textValue.toString());
			} else {
				aggregatePageRank = aggregatePageRank + Double.parseDouble(textValue.toString());
			}
		}
		
		double dampingFactor = ((1.0 - DAMPING_FACTOR) / (double) PageRankUtils.TOTAL_NODES_IN_GRAPH);
		
		double newPageRank = dampingFactor + (DAMPING_FACTOR * aggregatePageRank);
		
		if(node == null){
			System.out.println("=====================node null in reduce ===========================  "+ key);
		}
		double delta = Math.abs((node.getPageRank() - newPageRank))/newPageRank;
		
		node.setPageRank(newPageRank);
		
		
		context.write(new Text(node.serializeNode()),new Text()) ;
		int scaledDelta = Math.abs((int) (delta * CONVERGENCE_SCALING_FACTOR));
		context.getCounter(Counter.ERROR_MARGIN).increment(scaledDelta);
	}
	
	private boolean isNode(Text value){
		if(value.toString().trim().split(PageRankUtils.INPUT_TEXT_DELIMITER).length > 1){
			return true;
		}
		return false;
	}
}

