package iterativeMR;

import java.util.ArrayList;
import java.util.List;

public class Node {
	long nodeId = 0l;
	long blockId = 0l;
	List<Long> neighbourNodeIds = new ArrayList<Long>();  
	double pageRank = 0.0;
	
	public Node(long nodeId, long blockId, List<Long> neighbourNodeIds, double pageRank) {
		super();
		this.nodeId = nodeId;
		this.blockId = blockId;
		this.neighbourNodeIds = neighbourNodeIds;
		this.pageRank = pageRank;
	}
	
	public long getNodeId() {
		return nodeId;
	}
	public void setNodeId(long nodeId) {
		this.nodeId = nodeId;
	}
	public long getBlockId() {
		return blockId;
	}
	public void setBlockId(long blockId) {
		this.blockId = blockId;
	}
	public List<Long> getNeighbourNodeIds() {
		return neighbourNodeIds;
	}
	public void setNeighbourNodeIds(List<Long> neighbourNodeIds) {
		this.neighbourNodeIds = neighbourNodeIds;
	}
	public double getPageRank() {
		return pageRank;
	}
	public void setPageRank(double pageRank) {
		this.pageRank = pageRank;
	}
	
	
	//nodeID blockID list,of,adj,nodeids pageRank
	
	public String serializeNode() {
		// TODO Auto-generated method stub
		String serializedNode = this.nodeId+PageRankUtils.INPUT_TEXT_DELIMITER+this.blockId+PageRankUtils.INPUT_TEXT_DELIMITER+
								PageRankUtils.convertLongListToString(this.neighbourNodeIds)+PageRankUtils.INPUT_TEXT_DELIMITER+
								this.pageRank;
		return serializedNode;
	}
	
}
