package JacobiBlockMR;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class JacobiBlockPageRankDriver {

	  public static void main(String... args) throws Exception {
	
		try{
		    String inputFile = args[0];
		    String outputDir = args[1];
		    int iterationCount = Integer.parseInt(args[2]);
		   // org.apache.log4j.BasicConfigurator.configure();
			System.setProperty("hadoop.home.dir", "/");

	    	runUntilConvergence(inputFile, outputDir,iterationCount);
		} catch (Exception exception){
			System.out.println("Invalid input arguments ! : Format is inputfilePath OutputFolderPath Number_Of_Iterations_To_Run");
			
		}
	  }

	  public static void runUntilConvergence(String input, String output, int iterationCountNew) throws Exception {

	    Configuration conf = new Configuration();
	    
	    Path outputPath = new Path(output);
	    outputPath.getFileSystem(conf).delete(outputPath, true);
	    outputPath.getFileSystem(conf).mkdirs(outputPath);

	    Path inputPath = new Path(input);

	    int iterationCount = 0;
	    double desiredConvergence = 0.001;

	    
	    
	    while (true) {

    	  if(iterationCount == iterationCountNew){
	    	  break;
	      }
	    	
	      Path jobOutputPath = new Path(outputPath, String.valueOf(iterationCount));
	      
	      System.out.println("======================================");
	      System.out.println("Current Iteration number:    " + (iterationCount+1));
	      System.out.println("Current Input path:   " + inputPath);
	      System.out.println("Current Output path:  " + jobOutputPath);
	      System.out.println("======================================");

	      if (runMRAndCheckConvergence(inputPath, jobOutputPath,iterationCount) < desiredConvergence) {
	    	  System.out.println("Convergence is below " + desiredConvergence + ", we're done");
	    	  break;
	      }
	      inputPath = jobOutputPath;
	      
	      
	      iterationCount++;
	    }
	  }


	  public static double runMRAndCheckConvergence(Path inputPath, Path outputPath, int iterationCount)
	      throws Exception {
	    Configuration conf = new Configuration();

	    Job job = Job.getInstance(conf, "PageRankJob_"+(iterationCount+1));
	    
	        
	    job.setJarByClass(JacobiBlockPageRankDriver.class);
	    job.setMapperClass(JacobiBlockPRMap.class);
	    job.setReducerClass(JacobiBlockPRReduce.class);
	    
	    job.setInputFormatClass(KeyValueTextInputFormat.class);

	    job.setMapOutputKeyClass(LongWritable.class);
	    job.setMapOutputValueClass(Text.class);

	    FileInputFormat.setInputPaths(job, inputPath);
	    FileOutputFormat.setOutputPath(job, outputPath);

	    if (!job.waitForCompletion(true)) {
	      throw new Exception("Job failed");
	    }

	    long summedConvergence = job.getCounters().findCounter(
	        JacobiBlockPRReduce.Counter.ERROR_MARGIN).getValue();
	    double convergence =
	        ((double) summedConvergence /
	            JacobiBlockPRReduce.CONVERGENCE_SCALING_FACTOR) /
	            (double) JacobiBlockPageRankUtils.TOTAL_NUMBER_OF_BLOCKS;

	    long reducedIterations= job.getCounters().findCounter(
		        JacobiBlockPRReduce.Counter.REDUCER_ITERATIONS).getValue();
	    long averageReducerIterations = reducedIterations/JacobiBlockPageRankUtils.TOTAL_NUMBER_OF_BLOCKS;
	    
	    
	    System.out.println("======================================");
	    System.out.println("Summed Residual Error got through countere for all blocks:  " + summedConvergence);
	    System.out.println("Average Residual Error scaled down:        " + convergence);
	    System.out.println("Average Number of Reducer Iterations run to converge:  " + averageReducerIterations);
	    System.out.println("======================================");
	  
	    job.getCounters().findCounter(JacobiBlockPRReduce.Counter.ERROR_MARGIN)
  		.setValue(0L);
	    job.getCounters().findCounter(JacobiBlockPRReduce.Counter.REDUCER_ITERATIONS)
  		.setValue(0L);
	    
	    return convergence;
	  }


	}
