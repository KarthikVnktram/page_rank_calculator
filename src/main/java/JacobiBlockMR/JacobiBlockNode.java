package JacobiBlockMR;

import java.util.ArrayList;
import java.util.List;

public class JacobiBlockNode {
	long nodeId = 0l;
	long blockId = 0l;
	List<Long> neighbourNodeIds = new ArrayList<Long>();  
	double pageRank = 0.0;
	
	public JacobiBlockNode(){
		
	}
	
	public JacobiBlockNode(long nodeId, long blockId, List<Long> neighbourNodeIds, double pageRank) {
		super();
		this.nodeId = nodeId;
		this.blockId = blockId;
		this.neighbourNodeIds = neighbourNodeIds;
		this.pageRank = pageRank;
	}
	
	public long getNodeId() {
		return nodeId;
	}
	public void setNodeId(long nodeId) {
		this.nodeId = nodeId;
	}
	public long getBlockId() {
		return blockId;
	}
	public void setBlockId(long blockId) {
		this.blockId = blockId;
	}
	public List<Long> getNeighbourNodeIds() {
		return neighbourNodeIds;
	}
	public void setNeighbourNodeIds(List<Long> neighbourNodeIds) {
		this.neighbourNodeIds = neighbourNodeIds;
	}
	public double getPageRank() {
		return pageRank;
	}
	public void setPageRank(double pageRank) {
		this.pageRank = pageRank;
	}
	
	
	//nodeID blockID list,of,adj,nodeids pageRank
	
	public String serializeNode() {
		// TODO Auto-generated method stub
		String serializedNode = this.nodeId+JacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+this.blockId+JacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+
								convertLongListToString(this.neighbourNodeIds)+JacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER+
								this.pageRank;
		return serializedNode;
	}

	//nodeID blockID list,of,adj,nodeids pageRank
	public JacobiBlockNode getNodeFromText(String inputText,JacobiBlockNode node){
		
		String[] splitText = inputText.split(JacobiBlockPageRankUtils.INPUT_TEXT_DELIMITER);
		long nodeId = Long.parseLong(splitText[0]);
		long blockId = Long.parseLong(splitText[1]);
		
		String[] adjList = splitText[2].split(JacobiBlockPageRankUtils.ADJACENCY_LIST_DELIMITER);
		List<Long> adjacencyList = new ArrayList<Long>();
		//try to use stream API of java 8
		for(String destNode:adjList){
			adjacencyList.add(Long.parseLong(destNode));
		}
		
		double pageRank = Double.parseDouble(splitText[3]);
		node.setNodeId(nodeId);
		node.setBlockId(blockId);
		node.setNeighbourNodeIds(adjacencyList);
		node.setPageRank(pageRank);
		
		return node;
		
	}

	public String convertLongListToString(List<Long> neighbourNodeIds){
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for(Long neighbourNode : neighbourNodeIds){
				stringBuilder.append(neighbourNode);
				if(neighbourNode == neighbourNodeIds.get(neighbourNodeIds.size()-1) == false){
					stringBuilder.append(JacobiBlockPageRankUtils.ADJACENCY_LIST_DELIMITER);
				}
		}
		return stringBuilder.toString();
		
	}
	
}
